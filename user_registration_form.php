<?php
	include('session.php');
	require_once 'DB_Functions.php';
	$db = new DB_Functions();

		
	$error=''; // Variable To Store Error Message
	
	if (isset($_POST['submit'])) {
		
	if (empty($_POST['status']) || empty($_POST['amount']) || empty($_POST['email']) || empty($_POST['t_type']) || empty($_POST['payment_type']) ||
	empty($_POST['t_id']) || empty($_POST['t_date']) || empty($_POST['unique_id'])) {
	$error = "all fields are mandatory";
	}
	else
	{
		$firstname = "";
        $lastname = "";
        $phone = "";
        $dob = "";
        $gender = "";
        $deactivation_date = "";
        $isDeleted = 2; // new user
        $device_id = "0";
        $device = "";
        $address = "";
		$itsid = "";
		$card_no = "xxx";

        $result = $db->storeUser($firstname, $lastname, $_POST['email'], $phone, $dob, $gender, $address, $_POST['unique_id'], $device_id, $_POST['t_date'],
		$_POST['t_type'], $_POST['payment_type'], $_POST['t_date'], $deactivation_date, $_POST['status'], $card_no, $_POST['amount'], $itsid, $isDeleted, $device);
        
        if (! $result)
        {
            $error = "error: Registration faild!";
			            
        }
        else
        {
			echo '<script type="text/javascript">', 'clearform();', '</script>';
            unset($_POST);
			unset($_REQUEST);
			header('Location: thankyou.php');
			exit;			
			$error = "Thank you!, Registration Successfull";
        }
	}
	echo "<h2>" . "----------( " . $error . " )----------" . "</h2>";
	}
?>
<!doctype html>
<html lang="en-US">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html">
  <title>Burhan Application Form</title>
  <!--<meta name="author" content="Jake Rocheleau">
  <link rel="shortcut icon" href="http://static.tmimgcdn.com/img/favicon.ico">
  <link rel="icon" href="http://static.tmimgcdn.com/img/favicon.ico">-->
  <link rel="stylesheet" type="text/css" media="all" href="../Lisaan-ud-Dawat/public/css/styles.css">
  <link rel="stylesheet" type="text/css" media="all" href="../Lisaan-ud-Dawat/public/css/switchery.min.css">
  <script type="text/javascript" src="../Lisaan-ud-Dawat/public/js/switchery.min.js"></script>
  
 </head>

<body>

	

  <div id="wrapper">
  
	<h1>Burhan Registration Form</h1>
    <div id="profile" style="margin: 5px 5px 0; border-bottom: 1px solid #648c3a;">
		<b id="welcome"> Welcome : <i><?php echo $login_session; ?></i></b>
		<b id="logout"><a href="logout.php">Log Out</a></b><br><br>
	</div>
  
  
  
  <form action="user_registration_form.php" method="post">
  <div class="col-2">
    <label>
      Status
      <input id="status" name="status" value="success" tabindex="1">
    </label>
  </div>
  <div class="col-2">
    <label>
      Amount
      <input value="3.0 USD" id="amount" name="amount" tabindex="2">
    </label>
  </div>
  <div class="col-3">
    <label>
      Email
      <input placeholder="example@mail.com" id="email" name="email" tabindex="3">
    </label>
  </div>
  <div class="col-3">
    <label>
      Transaction Type
      <input placeholder="example VISA , AXIB ?" id="t_type" name="t_type" tabindex="4">
    </label>
  </div>
  <div class="col-3">
    <label>
      Payment Type
      <select id="payment_type" name="payment_type" tabindex="5">
        <option value="DC">DC</option>
        <option value="CC">CC</option>
        <option value="NB">NB</option>
      </select>
    </label>
  </div>
  
    <div class="col-2">
    <label>
      Transaction Id
      <input placeholder="transaction id" id="t_id" name="t_id" tabindex="6">
    </label>
  </div>
  <div class="col-2">
    <label>
      Transaction Date
      <input placeholder="2015-05-23 10:29:30am" id="t_date" name="t_date" tabindex="7">
    </label>
  </div>
  <div class="col-2">
    <label>
      Unique Id
      <input placeholder="Paypal(payer_id) or PayUMoney(payuMoneyId)" id="unique_id" name="unique_id" tabindex="8">
    </label>
  </div>
  
  <div class="col-submit">
    <input name="submit" type="submit" value=" register " style="width: 20%; height: 40px; padding: 2px; border: 1px solid black, background-image: -moz-linear-gradient(#97c16b, #8ab959);
  background-image: -webkit-linear-gradient(#97c16b, #8ab959);
  background-image: linear-gradient(#97c16b, #8ab959);
  border-bottom: 1px solid #648c3a;
  cursor: pointer;
  color: #fff;" tabindex="9">
  </div>
  
  </form>
  </div>
<script type="text/javascript">
var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

elems.forEach(function(html) {
  var switchery = new Switchery(html);
});

function clearform()
{
    document.getElementById("email").value=""; //don't forget to set the textbox id
    document.getElementById("t_type").value="";
	document.getElementById("t_date").value="";
	document.getElementById("t_id").value="";
	document.getElementById("payment_type").value="";
	document.getElementById("unique_id").value="";
}

</script>
</body>
</html>