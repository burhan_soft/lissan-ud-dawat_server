<?php
        define("LOG_FILE", "./database_error.log");
        include 'process_pdt.function.php';
        require_once 'DB_Functions.php';
        $db = new DB_Functions();
        
        $payment_data = isset($_GET['tx'])
        ? process_pdt($_GET['tx'])
        : FALSE;
		
		if($payment_data && $payment_data["payer_status"] == "verified") {      
        $status = $payment_data["payment_status"];
        $amount = $payment_data["payment_fee"].$payment_data["mc_currency"];
        $txnid = $payment_data["txn_id"];
        $email = $payment_data["payer_email"];
        $mode = $payment_data["txn_type"];
        $bank_code = $payment_data["payment_type"];
        $card_no = "xxxx";        
        $tdate = $payment_data["payment_date"];
        $payuMoneyId = $payment_data["payer_id"];
        
                
        /*
         * foreach ($_POST as $key => $value)
         * {
         * echo $key." - ".$value." ,\n";
         *
         * }
         */
 

        $firstname = "";
        $lastname = "";
        $phone = "";
        $dob = "";
        $gender = "";
        $deactivation_date = "";
        $isDeleted = 2; // new user
        $device_id = "0";
        $device = "";
        $address = "";
		$itsid = "";

        $result = $db->storeUser($firstname, $lastname, $email, $phone, $dob, $gender, $address, $payuMoneyId, $device_id, $tdate, $mode, $bank_code, $txnid, $deactivation_date, $status, $card_no, $amount, $itsid, $isDeleted, $device);
        
        if (! $result)
        {
            echo "<h3> Registration faild!, contact support for help </h3>";
            error_log(date('[Y-m-d H:i e] ') . "user info not save!" . PHP_EOL, 3, LOG_FILE);
        }
        else
        {
            echo "<h3> Thank you!, Registration Successfull </h3>";
        }
        }else{
			echo "Something went wrong at server side, could you please send the email address, transaction id and date on tayeb@burhansoft.com.";
		} 

?>

<?php 
if($payment_data && $payment_data["payer_status"] == "verified"){
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<title>Lisaan_ud_dawat</title>
<link href="../Lisaan-ud-Dawat/public/css/application.css" media="all"
	rel="stylesheet" type="text/css">
<link href="../Lisaan-ud-Dawat/public/images/favicon.ico"
	rel="shortcut icon" type="image/vnd.microsoft.icon">
<style>
table, th, td {
    border: 1px solid black;
	background-color: transparent;
	border-color:black;
	margin-left: 30%;
	padding: 10px;
	}
	td{
		color: white;
		text-align: left;
	}
</style>
</head>
<body style="zoom: 1;">
	<div>
		<div>

			<div class="form-actions">
			<br>
				<div>
					<img src="../Lisaan-ud-Dawat/public/images/ic_launcher.png" />
				</div>
				
				<h1>Lisaan-ud-Dawat - Keyboard for Android and iPhone</h1>
				<br>
				<h3>Thank you!!!</h3>
				
				<h3>Your payment is successful.</h3>
				<br>
				
		<table border="1" style="width:40%" >
		<tr>
                <td colspan="2" style="text-align: left;">
				Now, download the application using below link and after installation verify the details in the application using above registered email.</td>				
              </tr>
			 <tr>
			 <td colspan="2" style="text-align: center;">
			 <a href="https://play.google.com/store/apps/details?id=com.burhan.soft.lisanuddawat">
				<img alt="Get it on Google Play" src="https://developer.android.com/images/brand/ar_generic_rgb_wo_60.png" /> </a>
			 </td>
			 </tr> 
              <tr>
                <td>Status</td>
                <td><?php echo $status; ?></td>		
              </tr>
              <tr>
                <td>Transaction Id</td>
                <td><?php echo $txnid; ?></td>		
             </tr>
              <tr>
                <td>Amount</td>
                <td><?php echo $amount; ?></td>		
             </tr>
			 
			  <tr>
                <td>Transaction Type</td>
                <td><?php echo $mode; ?></td>		
              </tr>
			  <tr>
                <td>Payment Type</td>
                <td><?php echo $bank_code; ?></td>		
              </tr>
			  <tr>
                <td>Email Registered</td>
                <td><?php echo $email; ?></td>		
              </tr>
			 
		</table>
		<br>
		<br>
			</div>
		</div>
	</div>
	<script src="../public/js/application.js" type="text/javascript"></script>
</body>
</html>
<?php
}
?>