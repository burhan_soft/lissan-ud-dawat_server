<?php

/**
 * @author Harshal Randhe
 * @copyright 2015
 */
class DB_Functions
{

    private $db;

    private $mysqli_conn;
    
    // put your code here
    // constructor
    function __construct()
    {
        require_once 'DB_Connect.php';
        // connecting to database
        $this->db = new DB_Connect();
        $this->mysqli_conn = $this->db->connect();
    }
    
    // destructor
    function __destruct()
    {}

    public function auto_commit_trans($mode){
        mysqli_autocommit($this->mysqli_conn, $mode);
    }
    
    public function begin_trans(){
        mysqli_begin_transaction($this->mysqli_conn);
    }
    
    public function commit_trans(){
        mysqli_commit($this->mysqli_conn);
    }
    public function rollback_trans(){
        mysqli_rollback($this->mysqli_conn);
    }

    /**
     * Storing new user
     * returns user details
     */
    public function storeUser($fname, $lname, $email, $phoneno, $dob, $gender, $address, $payuMoneyId, $imei, $datetime, $mode, $bank_code, $transuction_id, $deactivation_date, $status, $card_no, $amount, $itsid, $isDeleted, $device)
    {
        $uuid = uniqid('id', true);
        
        $result = mysqli_query($this->mysqli_conn,"INSERT INTO user_information(firstname, lastname, email, phone_no,dob,gender,address,payuMoneyId,deviceidentifier,transaction_date,mode,bank_code,transaction_id,deactivation_date,status,card_no,amount,itsid,isDeleted,device)
        VALUES('$fname','$lname','$email','$phoneno','$dob','$gender','$address','$payuMoneyId','$imei','$datetime','$mode','$bank_code','$transuction_id','$deactivation_date','$status','$card_no','$amount','$itsid','$isDeleted','$device')") or die(mysqli_error($this->mysqli_conn));
        
        // check for successful store
        if ($result)
        {
            // get user details
            $uid = mysqli_insert_id($this->mysqli_conn); // last inserted id
            $result = mysqli_query($this->mysqli_conn,"SELECT * FROM user_information WHERE id = $uid") or die(mysqli_error($this->mysqli_conn));
            ;
            // return user details
            return mysqli_fetch_array($result);
        }
        else
        {
            return false;
        }
    }

    /**
     * Get user by email and deviceid
     */
    public function getUserByEmailAndDeviceId($email, $device_id)
    {
        $result = mysqli_query($this->mysqli_conn,"SELECT * FROM user_information WHERE email = '$email' && deviceidentifier = '$device_id'") or die(mysqli_error($this->mysqli_conn));
        // check for result
        
        if ($result)
        {
            $no_of_rows = mysqli_num_rows($result);
            if ($no_of_rows > 0)
            {
                $result = mysqli_fetch_array($result);
                
                return $result;
            }
            else
            {
                // user not found
                return false;
            }
        }
        return false;
    }

    /**
     * Get user by email
     */
    public function getUserByEmail($email)
    {
        $result = mysqli_query($this->mysqli_conn,"SELECT * FROM user_information WHERE email = '$email' && isDeleted = 0 ") or die(mysqli_error($this->mysqli_conn));
        // check for result
        $no_of_rows = mysqli_num_rows($result);
        if ($no_of_rows > 0)
        {
            $result = mysqli_fetch_array($result);
            
            return $result;
        }
        else
        {
            // user not found
            return false;
        }
    }

    /**
     * Check email is existed or not
     */
    public function isEmailExisted($email)
    {
        $result = mysqli_query($this->mysqli_conn,"SELECT email FROM user_information WHERE email = '$email'") or die(mysqli_error($this->mysqli_conn));
        
        if ($result)
        {
            $no_of_rows = mysqli_num_rows($result);
            
            if ($no_of_rows > 0)
            {
                // user existed
                return true;
            }
            else
            {
                // user not existed
                return false;
            }
        }
        return false;
    }

    /**
     * Check imei is existed or not
     */
    public function isIMEIExisted($imei)
    {
        $result = mysqli_query($this->mysqli_conn,"SELECT deviceidentifier FROM user_information WHERE deviceidentifier = '$imei'") or die(mysqli_error($this->mysqli_conn));
        
        if ($result)
        {
            $no_of_rows = mysqli_num_rows($result);
            if ($no_of_rows > 0)
            {
                // user existed
                return true;
            }
            else
            {
                // user not existed
                return false;
            }
        }
        return false;
    }

    /**
     * Check if new user
     */
    public function isNewUser($email)
    {
        $delete = 2;
        $device_id = "0";
        $result = mysqli_query($this->mysqli_conn,"SELECT * FROM user_information WHERE email = '$email' && deviceidentifier = '$device_id' && isDeleted = '$delete'") or die(mysqli_error($this->mysqli_conn));
        
        if ($result)
        {
            $no_of_rows = mysqli_num_rows($result);
            if ($no_of_rows > 0)
            {
                // user existed
                
                return true;
            }
            else
            {
                // user not existed
                return false;
            }
        }
        return false;
    }

    /**
     * Check if account is deleted or not
     */
    public function isDeleted($email, $device_id)
    {
        $delete = 1;
        $result = mysqli_query($this->mysqli_conn,"SELECT * FROM user_information WHERE email = '$email' && deviceidentifier = '$device_id' && isDeleted = '$delete'") or die(mysqli_error($this->mysqli_conn));
        
        if ($result)
        {
            $no_of_rows = mysqli_num_rows($result);
            if ($no_of_rows > 0)
            {
                // user existed
                
                return true;
            }
            else
            {
                // user not existed
                return false;
            }
        }
        return false;
    }

    /**
     * Check if account is deleted or not
     */
    public function isNotDelete($email, $device_id)
    {
        $delete = 0;
        $result = mysqli_query($this->mysqli_conn,"SELECT * FROM user_information WHERE email = '$email' && deviceidentifier = '$device_id' && isDeleted = '$delete'") or die(mysqli_error($this->mysqli_conn));
        
        if ($result)
        {
            $no_of_rows = mysqli_num_rows($result);
            if ($no_of_rows > 0)
            {
                // user existed
                
                return true;
            }
            else
            {
                // user not existed
                return false;
            }
        }
        return false;
    }
	
	/**
     * verify renew user is exist.
     */
    public function isReNewUserExist($email)
    {
        $delete = 0;
        $result = mysqli_query($this->mysqli_conn,"SELECT * FROM user_information WHERE email = '$email' && isDeleted = '$delete'") or die(mysqli_error($this->mysqli_conn));
        
        if ($result)
        {
            $no_of_rows = mysqli_num_rows($result);
            if ($no_of_rows > 0)
            {
                // user existed
                
                return true;
            }
            else
            {
                // user not existed
                return false;
            }
        }
        return false;
    }
	
	/**
     * check user subscription is expire.
     */
    public function userSubscription($t_date)
    {
		$days = 349;
		$curr_date = date("Y-m-d h:i:sa");
		$dStart = new DateTime($t_date);
		$dEnd  = new DateTime($curr_date);
		$dDiff = $dStart->diff($dEnd);
		//echo $dDiff->format('%R'); // use for point out relation: smaller/greater
		//echo $dDiff->days;
		
			if($dDiff->days > $days){
				
				return 365-$dDiff->days; 
			}else if($dDiff->days > 365){
				return -2;
			}else{
				return -1;
			}
		}	
		
    /**
     * register user after payment
     */
    public function register($email, $fname, $lname, $phone, $imei, $address, $dob, $gender, $itsid, $device)
    {
        $delete = 1;
        $device_id = "0";
        
        $result = mysqli_query($this->mysqli_conn,"UPDATE user_information SET firstname = '$fname' , lastname = '$lname' , phone_no = '$phone' , deviceidentifier = '$imei' , address = '$address' , dob = '$dob' , gender = '$gender' , isDeleted = 0 , itsid = '$itsid', device = '$device' WHERE email = '$email' && isDeleted = 2 && deviceidentifier = '$device_id'") or die(mysqli_error($this->mysqli_conn));
        
        if ($result)
        {
            // user existed
            return true;
        }
        // user device switched
        return false;
    }

    /**
     * set user account deactivate
     */
    public function setDeactive($email, $imei)
    {
        $delete = 1;
        // $date = ();
        
        $result = mysqli_query($this->mysqli_conn,"UPDATE user_information SET isDeleted = '$delete' , deactivation_date = now() WHERE email = '$email' && isDeleted = 0") or die(mysqli_error());
        
        if ($result)
        {
            
            // user existed
            return true;
        }
        // user device switched
        return false;
    }
	
	 /**
     * renew user account
     */
    public function reNewUser($email,$t_id,$t_date,$payuMoneyId)
    {
        $delete = 0;
        // $date = ();
        
        $result = mysqli_query($this->mysqli_conn,"UPDATE user_information SET transaction_id = '$t_id' , transaction_date = '$t_date' , payuMoneyId = '$payuMoneyId' WHERE email = '$email' && isDeleted = '$delete' ") or die(mysqli_error());
        
        if ($result)
        {
            
            // user renew
            return true;
        }
        // user fail to renew
        return false;
    }

    public function generateResponse($response, $user_data, $user_sub)
    {
        $response["email"] = $user_data['email'];
        $response["imei"] = $user_data['deviceidentifier'];
        $response["status"] = $user_data['status'];
        $response["fname"] = $user_data['firstname'];
        $response["lname"] = $user_data['lastname'];
        $response["ph"] = $user_data['phone_no'];
        $response["dob"] = $user_data['dob'];
        $response["gender"] = $user_data['gender'];
        $response["add"] = $user_data['address'];
        $response["amt"] = $user_data['amount'];
        $response["transid"] = $user_data['transaction_id'];
        $response["transdate"] = $user_data['transaction_date'];
        $response["bcode"] = $user_data['bank_code'];
        $response["mode"] = $user_data['mode'];
        $response["cno"] = $user_data['card_no'];
		$response["expiry"] = $user_sub;
        return $response;
    }
	/**
     * simple query browser
     */
    public function getQueryData($query)
    {
        
        $result = mysqli_query($this->mysqli_conn,$query) or die(mysqli_error($this->mysqli_conn));
    
        
        return $result;
    }
}

?>