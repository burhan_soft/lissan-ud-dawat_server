<?php

/**
 * Processes a PDT transaction id.
 *
 * @author     Torleif Berger
 * @link       http://www.geekality.net/?p=1210
 * @license    http://creativecommons.org/licenses/by/3.0/
 * @return     The payment data if $tx was valid; otherwise FALSE.
 */
 define("LOG_FILE", "./database_error.log");
 
function process_pdt($tx)
{
        // Init cURL
        $request = curl_init();
$identity_token = "ig6Xe1tPAyOHH7ZuuyJ7MqfQiu1HZMNuU-agCoHzb7E9iBDEt5v7Lyqqc4S";
        // Set request options
        curl_setopt_array($request, array
        (
                CURLOPT_URL => 'https://www.paypal.com/cgi-bin/webscr',
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => http_build_query(array
                (
                        'cmd' => '_notify-synch',
                        'tx' => $tx,
                        'at' => $identity_token,
                )),
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_SSL_VERIFYPEER => TRUE,
                CURLOPT_CAINFO => 'cacert.pem',
        ));

        // Execute request and get response and status code
        $response = curl_exec($request);
		 error_log(date('[Y-m-d H:i e] ') . "Response = ".$response . PHP_EOL, 3, LOG_FILE);
        $status   = curl_getinfo($request, CURLINFO_HTTP_CODE);
error_log(date('[Y-m-d H:i e] ') . "Status = ".$status . PHP_EOL, 3, LOG_FILE);
        // Close connection
        curl_close($request);

        // Validate response
        if($status == 200 AND strpos($response, 'SUCCESS') === 0)
        {
                // Remove SUCCESS part (7 characters long)
                $response = substr($response, 7);

                // Urldecode it
                $response = urldecode($response);

                // Turn it into associative array
                preg_match_all('/^([^=\r\n]++)=(.*+)/m', $response, $m, PREG_PATTERN_ORDER);
                $response = array_combine($m[1], $m[2]);

                // Fix character encoding if needed
                if(isset($response['charset']) AND strtoupper($response['charset']) !== 'UTF-8')
                {
                        foreach($response as $key => &$value)
                        {
                                $value = mb_convert_encoding($value, 'UTF-8', $response['charset']);
                        }

                        $response['charset_original'] = $response['charset'];
                        $response['charset'] = 'UTF-8';
                }

                // Sort on keys
                ksort($response);

                // Done!
                return $response;
        }

        return FALSE;
}