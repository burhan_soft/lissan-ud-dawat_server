<?php

/**
 * @author Harshal Randhe
 * @copyright 2015
 */

/**
 * File to handle all API requests
 * Accepts GET and POST
 *
 * Each request will be identified by TAG
 * Response will be JSON data
 *
 * /**
 * check for POST request
 */
date_default_timezone_set("Asia/Kolkata");
header('Content-Type: application/json; charset=utf-8');
define("LOG_FILE", "./database_error.log");
// include db handler
require_once 'DB_Functions.php';
$db = new DB_Functions();

if (isset($_POST["json"]))
{
    $data = json_decode($_POST["json"]);
    
    $emailExisted = $db->isEmailExisted($data->email);
    $imeiExisted = $db->isIMEIExisted($data->imei);
    $newUser = $db->isNewUser($data->email);
    $isNotDelete = $db->isNotDelete($data->email, $data->imei);
    
	/*error_log(date('[Y-m-d H:i e] ') . "emailExisted: $data->imei" . PHP_EOL, 3, LOG_FILE);
    error_log(date('[Y-m-d H:i e] ') . "emailExisted: $emailExisted" . PHP_EOL, 3, LOG_FILE);
    error_log(date('[Y-m-d H:i e] ') . "imeiExisted: $imeiExisted" . PHP_EOL, 3, LOG_FILE);
    error_log(date('[Y-m-d H:i e] ') . "newUser: $newUser" . PHP_EOL, 3, LOG_FILE);
    error_log(date('[Y-m-d H:i e] ') . "isNotDelete: $isNotDelete" . PHP_EOL, 3, LOG_FILE);*/
    
    $SUCCESS = 0;
    $ERROR = 1;
    $DEACTIVATED_USER = 2;
    $DEACTIVATE_BY_USER = 3;
    $NOT_REGISTER = 4;
    
    $VERIFY_TAG = '1';
    $DEACTIVATE_TAG = '2';
    
    // check for tag type
    if ($data->tag == $VERIFY_TAG)
    {
        // Request type is verification user
        $response["resp"] = $ERROR;
        $response["errmsg"] = "Error while verifying the user details.";
        $response["tag"] = $data->tag;
        
        /**
         * If user is not present with the provided email address, means the user is not paid for the application.
         */
        // email = false;
        if (! $emailExisted)
        {
			//error_log(date('[Y-m-d H:i e] ') . "if (! $emailExisted)" . PHP_EOL, 3, LOG_FILE);
            // user not exist
            $response["resp"] = $NOT_REGISTER;
            $response["errmsg"] = "The user is not register for the application.";
        }
        // email = true ; device_id = true; isDeleted = false; newUser = false;
        else if ($emailExisted && $imeiExisted && $isNotDelete && !$newUser)
        {
			//error_log(date('[Y-m-d H:i e] ') . "else if ($emailExisted && $imeiExisted && $isNotDelete && !$newUser)" . PHP_EOL, 3, LOG_FILE);
            $user_data = $db->getUserByEmailAndDeviceId($data->email, $data->imei);
            
            if ($user_data)
            {
                $response["resp"] = $SUCCESS;
				$response["errmsg"] = "";
                $response = $db->generateResponse($response, $user_data, $db->userSubscription($user_data['transaction_date']));
            }
        }
        // email = true ; device_id = false; isDeleted = false; newUser = false;
        else if ($emailExisted && !$imeiExisted && !$isNotDelete && !$newUser)
        {
			//error_log(date('[Y-m-d H:i e] ') . " else if ($emailExisted && !$imeiExisted && !$isNotDelete && !$newUser)" . PHP_EOL, 3, LOG_FILE);
            $user_data = $db->getUserByEmail($data->email);
            
            if ($user_data)
            {
                $response["resp"] = $DEACTIVATE_BY_USER;
                $response["errmsg"] = "Ask user to deactivate othe device.";
                $response = $db->generateResponse($response, $user_data, $db->userSubscription($user_data['transaction_date']));
            }
        }
        // email = true ; device_id = true; isDeleted = true; newUser = false;
        else if ($emailExisted && $imeiExisted && !$isNotDelete && !$newUser)
        {
			//error_log(date('[Y-m-d H:i e] ') . " else if ($emailExisted && $imeiExisted && !$isNotDelete && !$newUser)" . PHP_EOL, 3, LOG_FILE);
			//error_log(date('[Y-m-d H:i e] ') . "Inside Deactivate" . PHP_EOL, 3, LOG_FILE);
            $user_data = $db->getUserByEmailAndDeviceId($data->email, $data->imei);
            if ($user_data)
            {
                $response["resp"] = $DEACTIVATED_USER;
                $response["errmsg"] = "user account deactivated";
                $response = $db->generateResponse($response, $user_data,$db->userSubscription($user_data['transaction_date']));
            }
        }
        // email = true ; device_id = false; isDeleted = false; newUser = true;
        else if ($emailExisted && !$imeiExisted && !$isNotDelete && $newUser)
        {
			error_log(date('[Y-m-d H:i e] ') . "else if ($emailExisted && !$imeiExisted && !$isNotDelete && $newUser)" . PHP_EOL, 3, LOG_FILE);
            $result = $db->register($data->email, $data->fname, $data->lname, $data->ph, $data->imei, $data->add, $data->dob, $data->gender, $data->itsid, $data->device);
            if ($result)
            {
                $user_data = $db->getUserByEmailAndDeviceId($data->email, $data->imei);
                if ($user_data)
                {
                    $response["resp"] = $SUCCESS;
					$response["errmsg"] = "";
                    $response = $db->generateResponse($response, $user_data,$db->userSubscription($user_data['transaction_date']));
                }
				else 
					error_log(date('[Y-m-d H:i e] ') . "Failed getUserByEmailAndDeviceId register" . PHP_EOL, 3, LOG_FILE);
            }
			else 
				error_log(date('[Y-m-d H:i e] ') . "Failed to register" . PHP_EOL, 3, LOG_FILE);
        }
		else
			error_log(date('[Y-m-d H:i e] ') . "No conditions match" . PHP_EOL, 3, LOG_FILE);
			echo json_encode($response);
    }
    // Deactive user other device.
    else if ($data->tag == $DEACTIVATE_TAG)
    {
		$db->auto_commit_trans(false);  // set mysqli_autocommit false;
		
        $response["resp"] = $ERROR;
        $response["tag"] = $data->tag;
        $response["errmsg"] = "Error while deactivating user.";
		
        
        $data_row = $db->getUserByEmail($data->email);
		
		$db->begin_trans();  // transaction begin.
		
        if ($db->setDeactive($data->email, $data->imei))
        {
            $deactivation_date = "";
            $isDeleted = 0;
            
            // create new row with new device id other user info are same
            $user_data = $db->storeUser($data->fname, $data->lname, $data_row['email'], $data->ph, $data->dob, $data->gender, $data->add, $data_row['payuMoneyId'], $data->imei, $data_row['transaction_date'], $data_row['mode'], $data_row['bank_code'], $data_row['transaction_id'], $deactivation_date, $data_row['status'], $data_row['card_no'], $data_row['amount'], $isDeleted, $data->device);
            
            if ($user_data)
            {
                // data added!
                $response["resp"] = $SUCCESS;
				$response["errmsg"] = "";
                $response = $db->generateResponse($response, $user_data, $db->userSubscription($user_data['transaction_date']));
                $db->commit_trans();  // commite transaction.
            }else{
                $db->rollback_trans(); // if error rollback transaction.
            }
        }
       
        echo json_encode($response);
    }
    else
    {
        
        $response["resp"] = $ERROR;
        $response["tag"] = $data->tag;
        $response["errmsg"] = "Invalid Request";
        echo json_encode($response);
    }
}
else
{
    $response["resp"] = $ERROR;
    $response["errmsg"] = "parameter json missing";
    echo json_encode($response);
}

?>