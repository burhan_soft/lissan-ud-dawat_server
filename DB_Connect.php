<?php

/**
 * @author Harshal Randhe
 * @copyright 2015
 */
class DB_Connect
{
    
    // constructor
    function __construct()
    {}
    
    // destructor
    function __destruct()
    {
        // $this->close();
    }
    
    // Connecting to database
    public function connect()
    {
        /**
         * Database config variables
         */
        define('USERS_TABLE', 'user_information');
        
        define("DB_HOST", "localhost");
        define("DB_USER", "root");
        define("DB_PASSWORD", "");
        define("DB_DATABASE", "lisaan_ud_dawat");
		
		       
		$adminloginTableCreateQuery = "CREATE TABLE IF NOT EXISTS login(`id` int(10) NOT NULL AUTO_INCREMENT,
											`username` varchar(255) NOT NULL,
											`password` varchar(255) NOT NULL,
											PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";											
											
        $usersTableCreateQuery = "CREATE TABLE IF NOT EXISTS user_information (`id` int(11) NOT NULL AUTO_INCREMENT,
           `firstname` varchar(50) DEFAULT NULL,
           `lastname` varchar(50) DEFAULT NULL,
           `email` varchar(50) NOT NULL,
           `phone_no` varchar(20) DEFAULT NULL,
		   `dob` varchar(15) DEFAULT NULL,
           `gender` varchar(10) DEFAULT NULL,
		   `address` varchar(100) DEFAULT NULL,
           `payuMoneyId` varchar(30) NOT NULL,
           `deviceidentifier` varchar(50) DEFAULT NULL,
           `transaction_date` datetime NOT NULL,
           `bank_code` varchar(10) NOT NULL,
           `mode` varchar(10) NOT NULL,
           `transaction_id` varchar(50) NOT NULL,
           `deactivation_date` datetime DEFAULT NULL,
           `status` varchar(10) NOT NULL,
		   `card_no` varchar(50) NOT NULL,
		   `amount` varchar(10) NOT NULL,
		   `itsid` varchar(20) DEFAULT NULL,
           `isDeleted` tinyint(1) NOT NULL,
		   `device` varchar(10) DEFAULT NULL,
            PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        
        // connecting to mysql
        $con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD) or die("Unable to connect to MySQL");
        
        $sql = 'CREATE DATABASE IF NOT EXISTS lisaan_ud_dawat';
        if (mysqli_query($con,$sql))
        {
            // echo "Database my_db created successfully\n";
        }
        else
        {
            // echo 'Error creating database: ' . mysql_error() . "\n";
        }
        
        // selecting database
        mysqli_select_db($con,DB_DATABASE) or die("Could not select database");
        
        mysqli_query($con,$usersTableCreateQuery);
		mysqli_query($con,$adminloginTableCreateQuery);
        
        // return database handler
        return $con;
    }
    
    // Closing database connection
    public function close()
    {
        mysqli_close();
    }
	
}

?>